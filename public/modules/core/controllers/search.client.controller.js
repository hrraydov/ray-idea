'use strict';

angular.module('core').controller('SearchController', ['$scope', '$stateParams', '$http',
	function($scope, $stateParams, $http) {
		$scope.loadingUsers = true;
		$scope.loadingProjects = true;
		$scope.users = {
			data: [],
			pageCount: 1,
			page: 1
		};
		$scope.projects = {
			data: [],
			pageCount: 1,
			page: 1
		};

		var loadUsers = function(){
			$scope.loadingUsers = true;
			var q = '';
			if(!!$stateParams.q){
				q=$stateParams.q;
			}
			$http.get('/users/search?q=' + q + '&page=' + $scope.users.page)
			.success(function(data){
				$scope.users.data = data.users;
				$scope.users.pageCount = Math.floor(data.count / 10) + 1;
				$scope.loadingUsers = false;
			});
		};

		var loadProjects = function(){
			$scope.loadingProjects = true;
			var q = '';
			if(!!$stateParams.q){
				q=$stateParams.q;
			}
			$http.get('/projects/search?q=' + q + '&page=' + $scope.projects.page)
			.success(function(data){
				$scope.projects.data = data.projects;
				$scope.projects.pageCount = Math.floor(data.count / 10) + 1;
				$scope.loadingProjects = false;
			});
		};

		loadUsers();
		loadProjects();

		$scope.changeUsersPage = function(page){
			$scope.users.page = page;
			loadUsers();
		};

		$scope.changeProjectsPage = function(page){
			$scope.projects.page = page;
			loadProjects();
		};
	}
]);