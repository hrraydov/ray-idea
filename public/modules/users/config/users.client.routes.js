'use strict';

// Setting up route
angular.module('users').config(['$stateProvider',
	function($stateProvider) {
		// Users state routing
		$stateProvider.
		state('info', {
			url: '/edit-info',
			templateUrl: 'modules/users/views/edit-info.client.view.html'
		}).
		state('avatar', {
			url: '/edit-avatar',
			templateUrl: 'modules/users/views/edit-avatar.client.view.html'
		}).
		state('education', {
			url: '/edit-education',
			templateUrl: 'modules/users/views/edit-education.client.view.html'
		}).
		state('experience', {
			url: '/edit-experience',
			templateUrl: 'modules/users/views/edit-experience.client.view.html'
		}).
		state('best-positions', {
			url: '/best-positions',
			templateUrl: 'modules/users/views/best-positions.client.view.html'
		}).
		state('skills', {
			url: '/edit-skills',
			templateUrl: 'modules/users/views/edit-skills.client.view.html'
		}).
		state('user-show', {
			url: '/user/:username',
			templateUrl: 'modules/users/views/show-user/show-user.client.view.html'
		}).
		state('user-show.info', {
			url: '/info',
			templateUrl: 'modules/users/views/show-user/show-user-info.client.view.html'
		}).
		state('user-show.skills', {
			url: '/skills',
			templateUrl: 'modules/users/views/show-user/show-user-skills.client.view.html'
		}).
		state('user-show.education', {
			url: '/education',
			templateUrl: 'modules/users/views/show-user/show-user-education.client.view.html'
		}).
		state('user-show.experience', {
			url: '/experience',
			templateUrl: 'modules/users/views/show-user/show-user-experience.client.view.html'
		}).
		state('profile', {
			url: '/settings/profile',
			templateUrl: 'modules/users/views/settings/edit-profile.client.view.html'
		}).
		state('change-password', {
			url: '/change-password',
			templateUrl: 'modules/users/views/change-password.client.view.html'
		}).
		state('accounts', {
			url: '/settings/accounts',
			templateUrl: 'modules/users/views/settings/social-accounts.client.view.html'
		}).
		state('signup', {
			url: '/signup',
			templateUrl: 'modules/users/views/authentication/signup.client.view.html'
		}).
		state('signin', {
			url: '/signin',
			templateUrl: 'modules/users/views/authentication/signin.client.view.html'
		}).
		state('forgot', {
			url: '/password/forgot',
			templateUrl: 'modules/users/views/password/forgot-password.client.view.html'
		}).
		state('reset-invalid', {
			url: '/password/reset/invalid',
			templateUrl: 'modules/users/views/password/reset-password-invalid.client.view.html'
		}).
		state('reset-success', {
			url: '/password/reset/success',
			templateUrl: 'modules/users/views/password/reset-password-success.client.view.html'
		}).
		state('reset', {
			url: '/password/reset/:token',
			templateUrl: 'modules/users/views/password/reset-password.client.view.html'
		});
	}
]);