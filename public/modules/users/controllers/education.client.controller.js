'use strict';

angular.module('users').controller('EducationController', ['$scope', '$http', '$location', '$window', 'Authentication',
	function($scope, $http, $location, $window, Authentication) {
		console.log('EducationCOntroller loaded');

		if(!Authentication.user){
			$location.path('/');
		}

		$scope.loading = true;
		$scope.education = [];
		$scope.newEducation = {};
		$scope.errorMessage = null;

		$scope.loadEducation = function(){
			console.log('loadEducation() started');
			$scope.loading = true;
			$http.get('/users/' + Authentication.user._id + '/education')
			.success(function(data){
				$scope.education = data;
				$scope.loading = false;
				$scope.errorMessage = null;
			});
		};

		$scope.addEducation = function(){
			$http.post('/education', $scope.newEducation)
			.success(function(data){
				$scope.loadEducation();
				$scope.newEducation = {};
			})
			.error(function(data){
				$scope.errorMessage = data.message;
			});
		};

		$scope.removeEducation = function(education){
			$http.delete('/education/' + education._id)
			.success(function(data){
				$scope.loadEducation();
			});
		};

		$scope.editEducation = function(education){
			$http.put('/education/' + education._id, education)
			.success(function(data){
				$scope.loadEducation();
			})
			.error(function(data){
				$scope.errorMessage = data.message;
			});
		};
	}
]);