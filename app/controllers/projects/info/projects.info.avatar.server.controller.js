'use strict';

/**
 * Module dependencies
 */

var Project = require('mongoose').model('Project'),
	errorHandler = require('./../../errors.server.controller.js'),
	async = require('async'),
	multiparty = require('multiparty'),
	cloudinary = require('cloudinary'),
	_ = require('lodash');

/**
 * Get project avatar
 */
exports.getAvatar = function(req, res){
	var project = req.project;
	console.log(project.avatar);
	return res.status(200).send(project.avatar);
};

/**
 * Upload new avatar
 */
exports.uploadAvatar = function(req, res){
	var form = new multiparty.Form();
	var project = req.project;
    form.parse(req, function(err, fields, files) {
    	var file = files.file[0];
    	var contentType = file.headers['content-type'];
    	var extension = file.path.substring(file.path.lastIndexOf('.'));
    	if(!/jpg|jpeg|png/.test(extension)){
    		return res.status(400).send({
    			message: 'Only JPEG ang PNG files'
    		});
    	}
    	async.waterfall([
    		function(cb){
    			if(project.avatar !== '/modules/users/img/default_avatar.jpg'){
					cloudinary.api.delete_resources([project._id],
	    			function(result){ cb(); }, { keep_original: true });
				}
    		},
    		function(cb){    			
				cloudinary.uploader.upload(file.path, function(result){					
					project.avatar = result.url;
					project.save(function(err, project){
						cb();
					});
				}, {public_id: project._id, angle: 'exif'});
    		}
    	], function(err){
    		return res.json({});
    	});
    });
};

/**
 * Change avatar link
 */
exports.changeAvatarLink = function(req, res){
	var project = req.project;
	var link = req.body.link;

	if(project.avatar !== '/modules/users/img/default_avatar.jpg'){
		cloudinary.api.delete_resources([project._id],
		function(result){}, { keep_original: true });
	}
	project.avatar = link;
	project.save(function(err, project){
		return res.json({});
	});
};