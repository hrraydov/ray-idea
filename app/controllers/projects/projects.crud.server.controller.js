'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
	errorHandler = require('../errors.server.controller'),
	Project = mongoose.model('Project'),
	User = mongoose.model('User'),
	async = require('async'),
	cloudinary = require('cloudinary'),
	multiparty = require('multiparty'),
	_ = require('lodash');

/**
 * Create a Project
 */
exports.create = function(req, res) {
	var user = req.user;
	var project = new Project(req.body);
	project.urlName = project.name.replace(/\s+/g, '-', '-').toLowerCase() + '-' + Date.now();
	project.user = user._id;
	project.save(function(err, project) {
		if (err) {
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		} else {
			user.ownerOf.push(project);
			user.save(function(err){
				if(err){
					return res.status(400).send({
						message: errorHandler.getErrorMessage(err)
					});
				}
				return res.json(project);
			});
		}
	});
};


/**
 * Update a Project
 */
exports.update = function(req, res) {
	var project = req.project;

	project = _.extend(project , req.body);

	project.save(function(err) {
		if (err) {
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		} else {
			res.json(project);
		}
	});
};

/**
 * Delete a Project
 */
exports.delete = function(req, res) {
	var user = req.user;
	var project = req.project;

	project.remove(function(err){
		if(err){
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		}else{
			user.ownerOf.pull(project);
			user.save(function(err){
				if(err){
					return res.status(400).send({
						message: errorHandler.getErrorMessage(err)
					});
				}
				return res.json(project);
			});
		}
	});

	async.parallel([
		function(cb){
			project.remove(function(err){
			if(err){
				cb(err);
			}else{
				cb();
			}
	});
		},
		function(cb){
			user.ownerOf.pull(project);
			user.save(function(err){
				if(err){
					cb(err);
				}
				cb();
			});
		}
	], function(err){
		if(err){
			return res.status(400).send({message: errorHandler.getErrorMessage(err)});
		}
	});
};

/**
 * List of Created Projects
 */
exports.list = function(req, res) {
	var user = req.profile;
	var page = req.query.page;
	var pageSize = 5;

	Project
		.find({user: user._id})
		.limit(pageSize)
		.skip((page-1) * pageSize)
		.sort('-_id')
		.exec(function(err, projects){
			return res.json(projects);
		});	
};

/**
 * Count of created projects
 */
exports.count = function(req, res){
	var user = req.profile;
	Project.count({user: user._id}).exec(function(err, count){
		return res.json({count: count});
	});
};


exports.userById = function(req, res, next, id) { 
	User.findById(id).select('-password').exec(function(err, user) {
		if (err) return next(err);
		if (!user) return next(new Error('User not found' + id));
		req.userToWork = user;
		next();
	});
};
