'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
	Schema = mongoose.Schema;

/**
 * Education Schema
 */
var EducationSchema = new Schema({
	startYear: {
		type: Number,
		required: 'The start year is required'
	},
	endYear: Number,
	school: {
		type: String,
		required: 'The school name is required'
	},
	speciality: String,
	user: {
		type: Schema.Types.ObjectId,
		ref: 'User'
	}
});

mongoose.model('Education', EducationSchema);