'use strict';


angular.module('core').controller('HomeController', ['$scope', '$http', 'Authentication',
	function($scope, $http, Authentication) {
		// This provides Authentication context.
		$scope.authentication = Authentication;

		$scope.alerts = [];
		$scope.loadingAlerts = true;
		$scope.lastProjects = [];
		$scope.loadingLastProjects = true;

		$http.get('/alerts')
		.success(function(data){
			$scope.alerts = data;
			$scope.loadingAlerts = false;
		});

		$http.get('/projects/last')
		.success(function(data){
			$scope.lastProjects = data;
			$scope.loadingLastProjects = false;
		});
	}
]);