'use strict';

angular.module('users').controller('ShowUserAvatarController', ['$scope', '$http', '$window',
	function($scope, $http, $window, $stateParams) {

		$scope.avatar = {};
		$scope.loading = true;

		$scope.loadAvatar = function(){
			if($scope.userId){
				$scope.loading = true;
				$http.get('/users/' + $scope.userId + '/avatar')
				.success(function(data){
					$scope.loading = false;
					$scope.avatar = data;
				});
			}
		};

		$scope.$watch('userId', function(){
			$scope.loadAvatar();
		});
	}
]);