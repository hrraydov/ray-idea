'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
	Schema = mongoose.Schema,
	crypto = require('crypto');

/**
 * A Validation function for local strategy properties
 */
var validateLocalStrategyProperty = function(property) {
	return ((this.provider !== 'local' && !this.updated) || property.length);
};

/**
 * A Validation function for local strategy password
 */
var validateLocalStrategyPassword = function(password) {
	return (this.provider !== 'local' || (password && password.length > 6));
};

/**
 * User Schema
 */
var UserSchema = new Schema({
	firstName: {
		type: String,
		trim: true,
		default: '',
		validate: [validateLocalStrategyProperty, 'Попълни име']
	},
	lastName: {
		type: String,
		trim: true,
		default: '',
		validate: [validateLocalStrategyProperty, 'Попълни фамилия']
	},
	about: {
		type: String,
		trim: true,
		default: '',
	},
	gender: {
		type: String,
		trim: true,
		default: '',
		enum: ['', 'male', 'female']
	},	
    email: {
		type: String,
		trim: true,
		default: '',
		validate: [validateLocalStrategyProperty, 'Попълни email'],
		match: [/.+\@.+\..+/, 'Попълни валиден email'],
		unique: 'Email-а съществува'
	},
    birthdate: {
		type: String,
		trim: true,
		default: '',
	},
	displayName: {
		type: String,
		trim: true
	},
	website: {
		type: String,
		trim: true,
		default: '',
		match: [/^(https?\:\/\/)([\da-z\.-]+)\.([a-z\.]{2,6})([\/\w \.-]*)*\/?$/, 'Попълни валиден уебсайт']
	},
	username: {
		type: String,
		unique: 'Потребителското име съществува',
		required: 'Попълни потребителско име',
		trim: true
	},
	password: {
		type: String,
		default: '',
		validate: [validateLocalStrategyPassword, 'Паролата трябва да е над 6 символа']
	},
	salt: {
		type: String
	},
	provider: {
		type: String,
		required: 'Provider is required'
	},
	providerData: {},
	additionalProvidersData: {},
	roles: {
		type: [{
			type: String,
			enum: ['user', 'admin']
		}],
		default: ['user']
	},
	updated: {
		type: Date
	},
	created: {
		type: Date,
		default: Date.now
	},
	/* For reset password */
	resetPasswordToken: {
		type: String
	},
	resetPasswordExpires: {
		type: Date
	},
	education: {
		type: [{
			type: Schema.Types.ObjectId,
			ref: 'Education'
		}],
		default: []
	},
	experience: {
		type: [{
			type: Schema.Types.ObjectId,
			ref: 'Experience'
		}],
		default: []
	},
	skills: {
		type: [{
			name: String,			
			skill: {
				type: Schema.Types.ObjectId,
				ref: 'Skill'
			}
		}],
		default: []
	},
	ownerOf: {
		type: [{
			type: Schema.Types.ObjectId,
			ref: 'Project'
		}],
		default: []
	},
	memberOf: {
		type: [{
			type: Schema.Types.ObjectId,
			ref: 'Project'
		}],
		default: []
	},
	candidateFor: {
		type: [{
			type: Schema.Types.ObjectId,
			ref: 'Project'
		}],
		default: []
	},
	invitations: {
		type: [{
			type: Schema.Types.ObjectId,
			ref: 'Project'
		}],
		default: []
	},
	avatar: {
		type: String,
		default: '/modules/users/img/default_avatar.jpg'		
	}
},
{
	versionKey: false
});

/**
 * Hook a pre save method to hash the password
 */
UserSchema.pre('save', function(next) {
	if (this.password && this.password.length > 6) {
		this.salt = new Buffer(crypto.randomBytes(16).toString('base64'), 'base64');
		this.password = this.hashPassword(this.password);
	}

	next();
});

/**
 * Create instance method for hashing a password
 */
UserSchema.methods.hashPassword = function(password) {
	if (this.salt && password) {
		console.log(this.salt);
		console.log(password);
		console.log(crypto.pbkdf2Sync(password, this.salt, 10000, 64).toString('base64'));
		return crypto.pbkdf2Sync(password, this.salt, 10000, 64).toString('base64');
	} else {
		return password;
	}
};

/**
 * Create instance method for authenticating user
 */
UserSchema.methods.authenticate = function(password) {
	return this.password === this.hashPassword(password);
};

/**
 * Create instatnce method for getting public information
 */
UserSchema.methods.getPublicData = function(){
	return {
		firstName: this.firstName,
		lastName: this.lastName,
		about: this.about,
		birthdate: this.birthdate,
		education: this.education,
		experience: this.experience,
		displayName: this.displayName,
		website: this.website,
		gender: this.gender,
		username: this.username,
		skills: this.skills,
		ownerOf: this.ownerOf,
		memberOf: this.memberOf,
		avatar: this.avatar
	};
};

UserSchema.methods.getInfo = function(){
	return {
		firstName: this.firstName,
		lastName: this.lastName,
		about: this.about,
		birthdate: this.birthdate,
		website: this.website,
		gender: this.gender,
		username: this.username,
		email: this.email,
		displayName: this.displayName
	};
};

/**
 * Find possible not used username
 */
UserSchema.statics.findUniqueUsername = function(username, suffix, callback) {
	var _this = this;
	var possibleUsername = username + (suffix || '');

	_this.findOne({
		username: possibleUsername
	}, function(err, user) {
		if (!err) {
			if (!user) {
				callback(possibleUsername);
			} else {
				return _this.findUniqueUsername(username, (suffix || 0) + 1, callback);
			}
		} else {
			callback(null);
		}
	});
};

mongoose.model('User', UserSchema);