'use strict';

angular.module('core').directive('loader', [
	function() {
		return {
			template: '<div class="text-center"><img src="/modules/core/img/loaders/712.GIF" alt="alertLoading"/></div>',
			restrict: 'A',
			link: function postLink(scope, element, attrs) {
			}
		};
	}
]);