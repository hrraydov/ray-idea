'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
	errorHandler = require('../errors.server.controller'),
	Project = mongoose.model('Project'),
	async = require('async'),
	_ = require('lodash');

/**
 * Get projectId
 */
exports.getProjectId = function(req, res){
	var urlName = req.params.urlName;

	Project.findOne({urlName: urlName}).exec(function(err, project){
		if(err){
			return res.status(400).send(err);
		}
		if(!project){
			return res.status(404).send({message: 'project not found'});
		}
		return res.status(200).json({projectId: project._id});
	});
};

/**
 * Get user type
 */
exports.getUserType = function(req, res){
	var user = req.profile;
	var project = req.project;

	if(project.members.indexOf(user._id) !== -1){
		return res.json({type: 'member'});
	}else if(project.user.equals(user._id)){
		return res.json({type: 'owner'});
	}else{
		return res.json({type: 'sth else'});
	}
};

/**
 * Search projects
 */
exports.search = function(req, res){
	var q = req.query.q;
	var page = req.query.page;
	var result = {};
	async.parallel([
		function(callback){			
			Project
			.find({name: new RegExp(q, 'i')})
			.populate('user', 'username displayName')
			.select('name user urlName')
			.skip((page-1)*10)
			.limit(10)
			.sort('name')
			.exec(function(err, projects){
				result.projects = projects;
				callback();
			});
		},
		function(callback){
			Project.count({name: new RegExp(q, 'i')}, function(err, count){
				result.count = count;
				callback();
			});
		}
	], function(err){
		return res.json(result);
	});
};

/**
 * Get last projects
 */
exports.last = function(req, res){
	Project.find().sort('-_id').limit(5).exec(function(err, projects){
		return res.json(projects);
	});
};


/**
 * Project middleware
 */
exports.projectById = function(req, res, next, id) {
	console.log('Project id: ' + id);
	Project.findById(id).exec(function(err, project) {
		if (err) return next(err);
		if (! project) return next(new Error('Failed to load Project ' + id));
		req.project = project ;
		next();
	});
};

/**
 * Project authorization middleware
 */
exports.hasAuthorization = function(req, res, next) {
	if (!req.project.user.equals(req.user._id)) {
		return res.status(403).send('User is not authorized');
	}
	next();
};