'use strict';

angular.module('users').controller('AvatarController', ['$scope', '$http', '$location', '$window', 'FileUploader', 'Authentication',
	function($scope, $http, $location, $window, FileUploader, Authentication) {
		if(!Authentication.user){
			$location.path('/');
		}

		$scope.loading = true;
		$scope.avatar = {};
		$scope.errorMessage = null;
		$scope.successMessage = null;
		$scope.avatarUploader = new FileUploader({
			url: '/users/upload-avatar'
		});

		$scope.loadAvatar = function(){
			$scope.loading = true;
			$http.get('/users/' + Authentication.user._id + '/avatar')
			.success(function(data){
				console.log(data);
				$scope.avatar = data;
				$scope.loading = false;
				$scope.errorMessage = null;
			});
		};

		$scope.uploadAvatar = function(){
			$scope.loading = true;
			$scope.avatarUploader.uploadAll();
			$scope.avatarUploader.onCompleteAll = function() {				
				$scope.loadAvatar();
			};
		};

		$scope.changeAvatarLink = function(){
			$http.post('/users/change-avatar-link', {link: $scope.link})
			.success(function(data){
				$scope.loadAvatar();
			});
		};
		
	}
]);