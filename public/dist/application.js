'use strict';

// Init the application configuration module for AngularJS application
var ApplicationConfiguration = (function() {
	// Init module configuration options
	var applicationModuleName = 'mean';
	var applicationModuleVendorDependencies = ['angularFileUpload', 'ngResource', 'ngSanitize', 'ngAnimate', 'ui.router', 'ui.bootstrap', 'ui.utils'];

	// Add a new vertical module
	var registerModule = function(moduleName, dependencies) {
		// Create angular module
		angular.module(moduleName, dependencies || []);

		// Add the module to the AngularJS configuration file
		angular.module(applicationModuleName).requires.push(moduleName);
	};

	return {
		applicationModuleName: applicationModuleName,
		applicationModuleVendorDependencies: applicationModuleVendorDependencies,
		registerModule: registerModule
	};
})();
'use strict';

//Start by defining the main module and adding the module dependencies
angular.module(ApplicationConfiguration.applicationModuleName, ApplicationConfiguration.applicationModuleVendorDependencies);

// Setting HTML5 Location Mode
angular.module(ApplicationConfiguration.applicationModuleName).config(['$locationProvider',
	function($locationProvider) {
		$locationProvider.hashPrefix('!');
	}
]);

//Then define the init function for starting up the application
angular.element(document).ready(function() {
	//Fixing facebook bug with redirect
	if (window.location.hash === '#_=_') window.location.hash = '#!';

	//Then init the app
	angular.bootstrap(document, [ApplicationConfiguration.applicationModuleName]);
});
'use strict';

// Use Application configuration module to register a new module
ApplicationConfiguration.registerModule('core');

'use strict';

// Use applicaion configuration module to register a new module
ApplicationConfiguration.registerModule('projects');
'use strict';

// Use Application configuration module to register a new module
ApplicationConfiguration.registerModule('users');
'use strict';

// Setting up route
angular.module('core').config(['$stateProvider', '$urlRouterProvider',
	function($stateProvider, $urlRouterProvider) {
		// Redirect to home view when route not found
		$urlRouterProvider.otherwise('/');

		// Home state routing
		$stateProvider.
		state('search', {
			url: '/search?q&page',
			templateUrl: 'modules/core/views/search.client.view.html'
		}).
		state('home', {
			url: '/',
			templateUrl: 'modules/core/views/home.client.view.html'
		});
	}
]);
'use strict';

angular.module('core').controller('HeaderController', ['$scope', '$location', 'Authentication', 'Menus',
	function($scope, $location, Authentication, Menus) {
		$scope.authentication = Authentication;
		$scope.isCollapsed = false;
		$scope.q = '';
		$scope.menu = Menus.getMenu('topbar');

		$scope.toggleCollapsibleMenu = function() {
			$scope.isCollapsed = !$scope.isCollapsed;
		};

		// Collapsing the menu after navigation
		$scope.$on('$stateChangeSuccess', function() {
			$scope.isCollapsed = false;
		});

		$scope.search = function(){
			console.log(123);
			$location.path('/search').search({q: $scope.q});
		};
	}
]);
'use strict';


angular.module('core').controller('HomeController', ['$scope', '$http', 'Authentication',
	function($scope, $http, Authentication) {
		// This provides Authentication context.
		$scope.authentication = Authentication;

		$scope.alerts = [];
		$scope.loadingAlerts = true;
		$scope.lastProjects = [];
		$scope.loadingLastProjects = true;

		$http.get('/alerts')
		.success(function(data){
			$scope.alerts = data;
			$scope.loadingAlerts = false;
		});

		$http.get('/projects/last')
		.success(function(data){
			$scope.lastProjects = data;
			$scope.loadingLastProjects = false;
		});
	}
]);
'use strict';

angular.module('core').controller('SearchController', ['$scope', '$stateParams', '$http',
	function($scope, $stateParams, $http) {
		$scope.loadingUsers = true;
		$scope.loadingProjects = true;
		$scope.users = {
			data: [],
			pageCount: 1,
			page: 1
		};
		$scope.projects = {
			data: [],
			pageCount: 1,
			page: 1
		};

		var loadUsers = function(){
			$scope.loadingUsers = true;
			var q = '';
			if(!!$stateParams.q){
				q=$stateParams.q;
			}
			$http.get('/users/search?q=' + q + '&page=' + $scope.users.page)
			.success(function(data){
				$scope.users.data = data.users;
				$scope.users.pageCount = Math.floor(data.count / 10) + 1;
				$scope.loadingUsers = false;
			});
		};

		var loadProjects = function(){
			$scope.loadingProjects = true;
			var q = '';
			if(!!$stateParams.q){
				q=$stateParams.q;
			}
			$http.get('/projects/search?q=' + q + '&page=' + $scope.projects.page)
			.success(function(data){
				$scope.projects.data = data.projects;
				$scope.projects.pageCount = Math.floor(data.count / 10) + 1;
				$scope.loadingProjects = false;
			});
		};

		loadUsers();
		loadProjects();

		$scope.changeUsersPage = function(page){
			$scope.users.page = page;
			loadUsers();
		};

		$scope.changeProjectsPage = function(page){
			$scope.projects.page = page;
			loadProjects();
		};
	}
]);
'use strict';

angular.module('core').directive('loader', [
	function() {
		return {
			template: '<div class="text-center"><img src="/modules/core/img/loaders/712.GIF" alt="alertLoading"/></div>',
			restrict: 'A',
			link: function postLink(scope, element, attrs) {
			}
		};
	}
]);
'use strict';

//Menu service used for managing  menus
angular.module('core').service('Menus', [

	function() {
		// Define a set of default roles
		this.defaultRoles = ['*'];

		// Define the menus object
		this.menus = {};

		// A private function for rendering decision 
		var shouldRender = function(user) {
			if (user) {
				if (!!~this.roles.indexOf('*')) {
					return true;
				} else {
					for (var userRoleIndex in user.roles) {
						for (var roleIndex in this.roles) {
							if (this.roles[roleIndex] === user.roles[userRoleIndex]) {
								return true;
							}
						}
					}
				}
			} else {
				return this.isPublic;
			}

			return false;
		};

		// Validate menu existance
		this.validateMenuExistance = function(menuId) {
			if (menuId && menuId.length) {
				if (this.menus[menuId]) {
					return true;
				} else {
					throw new Error('Menu does not exists');
				}
			} else {
				throw new Error('MenuId was not provided');
			}

			return false;
		};

		// Get the menu object by menu id
		this.getMenu = function(menuId) {
			// Validate that the menu exists
			this.validateMenuExistance(menuId);

			// Return the menu object
			return this.menus[menuId];
		};

		// Add new menu object by menu id
		this.addMenu = function(menuId, isPublic, roles) {
			// Create the new menu
			this.menus[menuId] = {
				isPublic: isPublic || false,
				roles: roles || this.defaultRoles,
				items: [],
				shouldRender: shouldRender
			};

			// Return the menu object
			return this.menus[menuId];
		};

		// Remove existing menu object by menu id
		this.removeMenu = function(menuId) {
			// Validate that the menu exists
			this.validateMenuExistance(menuId);

			// Return the menu object
			delete this.menus[menuId];
		};

		// Add menu item object
		this.addMenuItem = function(menuId, menuItemTitle, menuItemURL, menuItemType, menuItemUIRoute, isPublic, roles, position) {
			// Validate that the menu exists
			this.validateMenuExistance(menuId);

			// Push new menu item
			this.menus[menuId].items.push({
				title: menuItemTitle,
				link: menuItemURL,
				menuItemType: menuItemType || 'item',
				menuItemClass: menuItemType,
				uiRoute: menuItemUIRoute || ('/' + menuItemURL),
				isPublic: ((isPublic === null || typeof isPublic === 'undefined') ? this.menus[menuId].isPublic : isPublic),
				roles: ((roles === null || typeof roles === 'undefined') ? this.menus[menuId].roles : roles),
				position: position || 0,
				items: [],
				shouldRender: shouldRender
			});

			// Return the menu object
			return this.menus[menuId];
		};

		// Add submenu item object
		this.addSubMenuItem = function(menuId, rootMenuItemURL, menuItemTitle, menuItemURL, menuItemUIRoute, isPublic, roles, position) {
			// Validate that the menu exists
			this.validateMenuExistance(menuId);

			// Search for menu item
			for (var itemIndex in this.menus[menuId].items) {
				if (this.menus[menuId].items[itemIndex].link === rootMenuItemURL) {
					// Push new submenu item
					this.menus[menuId].items[itemIndex].items.push({
						title: menuItemTitle,
						link: menuItemURL,
						uiRoute: menuItemUIRoute || ('/' + menuItemURL),
						isPublic: ((isPublic === null || typeof isPublic === 'undefined') ? this.menus[menuId].items[itemIndex].isPublic : isPublic),
						roles: ((roles === null || typeof roles === 'undefined') ? this.menus[menuId].items[itemIndex].roles : roles),
						position: position || 0,
						shouldRender: shouldRender
					});
				}
			}

			// Return the menu object
			return this.menus[menuId];
		};

		// Remove existing menu object by menu id
		this.removeMenuItem = function(menuId, menuItemURL) {
			// Validate that the menu exists
			this.validateMenuExistance(menuId);

			// Search for menu item to remove
			for (var itemIndex in this.menus[menuId].items) {
				if (this.menus[menuId].items[itemIndex].link === menuItemURL) {
					this.menus[menuId].items.splice(itemIndex, 1);
				}
			}

			// Return the menu object
			return this.menus[menuId];
		};

		// Remove existing menu object by menu id
		this.removeSubMenuItem = function(menuId, submenuItemURL) {
			// Validate that the menu exists
			this.validateMenuExistance(menuId);

			// Search for menu item to remove
			for (var itemIndex in this.menus[menuId].items) {
				for (var subitemIndex in this.menus[menuId].items[itemIndex].items) {
					if (this.menus[menuId].items[itemIndex].items[subitemIndex].link === submenuItemURL) {
						this.menus[menuId].items[itemIndex].items.splice(subitemIndex, 1);
					}
				}
			}

			// Return the menu object
			return this.menus[menuId];
		};

		//Adding the topbar menu
		this.addMenu('topbar');
	}
]);
'use strict';

// Configuring the Articles module
angular.module('projects').run(['Menus',
	function(Menus) {
		// Set top bar menu items
		Menus.addMenuItem('topbar', 'Проекти', 'projects');
	}
]);
'use strict';

//Setting up route
angular.module('projects').config(['$stateProvider',
	function($stateProvider) {
		// Projects state routing
		$stateProvider.
		state('listProjects', {
			url: '/projects',
			templateUrl: 'modules/projects/views/list-projects.client.view.html'
		}).
		state('createProject', {
			url: '/projects/create',
			templateUrl: 'modules/projects/views/create-project.client.view.html'
		}).
		state('viewProject', {
			url: '/projects/:urlName',
			templateUrl: 'modules/projects/views/view-project.client.view.html'
		}).
		state('editProject', {
			url: '/projects/:projectId/edit',
			templateUrl: 'modules/projects/views/edit-project.client.view.html'
		});
	}
]);
'use strict';

// Projects controller
angular.module('projects').controller('ProjectsController', ['$scope', '$stateParams', '$location', '$http', 'FileUploader', 'Authentication', 'Projects',
	function($scope, $stateParams, $location, $http, FileUploader, Authentication, Projects) {
		$scope.authentication = Authentication;
		$scope.loadingProject = true;
		$scope.project = {};
		$scope.activePosition = null;
		$scope.newRequiredSkill = {};
		$scope.newPlusSkill = {};
		$scope.newImage = null;
		$scope.newLink = {};
		$scope.uploader = new FileUploader();
		$scope.uploader.onCompleteAll = function(){
			loadProject();
		};
		$scope.avatarUploader = new FileUploader();
		$scope.avatarUploader.onCompleteAll = function(){
			loadProject();
		};

		// List Projects
		$scope.loadingProjects = true;		
		$scope.projects = {
			ownerOf: [],
			memberOf: [],
			candidateFor: [],
			invitations: []
		};
		$scope.owner = {
			page: 1,
			pageCount: 1
		};
		$scope.member = {
			page: 1,
			pageCount: 1
		};
		$scope.candidate = {
			page: 1,
			pageCount: 1
		};		
		$scope.invitation = {
			page: 1,
			pageCount: 1
		};
		$scope.changeOwnerPage = function(page){
			$scope.owner.page = page;
		};
		$scope.changeMemberPage = function(page){
			$scope.owner.page = page;
		};
		$scope.changeCandidatePage = function(page){
			$scope.owner.page = page;
		};
		$scope.changeInvitationPage = function(page){
			$scope.invitation.page = page;
		};
		$scope.getOwnerOf = function(page){
			return $scope.projects.ownerOf.slice((page-1)*5, 5);
		};
		$scope.getMemberOf = function(page){
			return $scope.projects.memberOf.slice((page-1)*5, 5);
		};
		$scope.getCandidateFor = function(page){
			return $scope.projects.candidateFor.slice((page-1)*5, 5);
		};
		$scope.getInvitations = function(page){
			return $scope.projects.invitations.slice((page-1)*5, 5);
		};
		$scope.newProject = {};
		var loadProjects = function(){
			$scope.loadingProjects = true;
			$http.get('/projects/user/'+Authentication.user.username).success(function(projects){
				$scope.projects = projects;
				$scope.owner.pageCount = Math.floor($scope.projects.ownerOf.length / 5) + 1;
				$scope.member.pageCount = Math.floor($scope.projects.memberOf.length / 5) + 1;
				$scope.candidate.pageCount = Math.floor($scope.projects.candidateFor.length / 5) + 1;
				$scope.invitation.pageCount = Math.floor($scope.projects.invitations.length / 5) + 1;
				$scope.loadingProjects = false;
			});
		};
		$scope.loadProjects = loadProjects;
		// Create new Project
		$scope.create = function() {
			// Create new Project object
			$http.post('/projects', $scope.newProject)
			.success(function(data){
				$scope.newProject = {};
				loadProjects();
			})
			.error(function(data){
				$scope.error = data.message;
			});
		};
		// Remove existing Project
		$scope.remove = function(project) {
			$http.delete('/projects/' + project._id)
			.success(function(){
				loadProjects();
			});
		};
		$scope.leave = function(project){
			$http.put('/projects/' + project._id + '/leave')
			.success(function(){
				loadProjects();
			});
		};
		$scope.cancelCandidature = function(project){
			$http.put('/projects/' + project._id + '/cancel-candidature')
			.success(function(){
				loadProjects();
			});
		};
		$scope.acceptInvitation = function(project){
			$http.put('/projects/' + project._id + '/accept-invitation')
			.success(function(){
				loadProjects();
			});
		};
		$scope.declineInvitation = function(project){
			$http.put('/projects/' + project._id + '/decline-invitation')
			.success(function(){
				loadProjects();
			});
		};


		// View Project
		$scope.loadingProject = true;
		var loadProject = function(){
			$scope.loadingProject = true;
			$http.get('/projects/' + $stateParams.urlName)
			.success(function(data){
				$scope.project = data.project;
				$scope.isOwner = data.isOwner;
				$scope.isMember = data.isMember;
				$scope.loadingProject = false;
				$scope.uploader.url = '/projects/' + $scope.project._id + '/image';
				$scope.avatarUploader.url = '/projects/' + $scope.project._id + '/avatar';
			});
		};

		$scope.showBecomeMemberBtn = function(){
			return Authentication.user && !$scope.isMember && !$scope.isOwner; 
		};

		// Update existing Project
		$scope.update = function() {
			var project = $scope.project;

			$http.put('/projects/' + project._id, $scope.project)
			.success(function(data){
				loadProject();
			});
		};

		$scope.loadProject = loadProject;

		$scope.becomeMember = function() {
			var project = $scope.project;

			$http.put('/projects/' + project._id + '/become-member')
			.success(function(data){
				loadProject();
			});
		};

		$scope.cancelCandidature = function(project) {
			$http.put('/projects/' + project._id + '/cancel-candidature')
			.success(function(data){
				loadProjects();
			});
		};

		$scope.acceptCandidature = function(candidate) {
			var project = $scope.project;

			$http.put('/projects/' + project._id + '/accept-candidature/' + candidate._id)
			.success(function(data){
				loadProject();
			});
		};

		$scope.declineCandidature = function(candidate) {
			var project = $scope.project;

			$http.put('/projects/' + project._id + '/decline-candidature/' + candidate._id)
			.success(function(data){
				loadProject();
			});
		};

		$scope.removeMember = function(member) {
			var project = $scope.project;

			$http.put('/projects/' + project._id + '/remove-member/' + member._id)
			.success(function(data){
				loadProject();
			});
		};

		$scope.addEmptyPosition = function(){
			$http.post('/projects/'+ $scope.project._id + '/position', {name: 'Position'})
			.success(function(data){
				loadProject();
				$scope.activePosition = data;
			});
		};

		$scope.updatePosition = function(){
			$http.put('/projects/' + $scope.project._id + '/position/' + $scope.activePosition._id, $scope.activePosition)
			.success(function(data){
				loadProject();
			});
		};

		$scope.selectPosition = function(position){
			$scope.activePosition = angular.copy(position);
			$http.get('/projects/' + $scope.project._id + '/position/' + $scope.activePosition._id + '/good-users')
			.success(function(data){
				$scope.goodUsers = data;
			});
		};

		$scope.removePosition = function(position){
			$http.delete('/projects/' + $scope.project._id + '/position/' + $scope.activePosition._id)
			.success(function(){
				$scope.activePosition = null;
				loadProject();
			});
		};

		$scope.addRequiredSkill = function(){
			$http.put('/projects/'+ $scope.project._id +'/position/' + $scope.activePosition._id + '/add-required', $scope.newRequiredSkill)
			.success(function(data){
				$scope.newRequiredSkill = {};
				$scope.activePosition = data;
				loadProject();
			});
		};

		$scope.addPlusSkill = function(){
			$http.put('/projects/'+ $scope.project._id +'/position/' + $scope.activePosition._id + '/add-plus', $scope.newPlusSkill)
			.success(function(data){
				$scope.newPlusSkill = {};
				$scope.activePosition = data;
				loadProject();
			});
		};

		$scope.removeRequiredSkill = function(skill){
			$http.put('/projects/'+ $scope.project._id +'/position/' + $scope.activePosition._id + '/remove-required', skill)
			.success(function(data){
				$scope.activePosition = data;
				loadProject();
			});
		};

		$scope.removePlusSkill = function(skill){
			$http.put('/projects/'+ $scope.project._id +'/position/' + $scope.activePosition._id + '/remove-plus', skill)
			.success(function(data){
				$scope.activePosition = data;
				loadProject();
			});
		};

		$scope.sendInvitation = function(user){
			$http.put('/projects/' + $scope.project._id + '/send-invitation/' + user._id)
			.success(function(){
				loadProject();
			});
		};

		$scope.uploadImage = function(){
			$scope.uploader.uploadAll();
		};

		$scope.uploadAvatar = function(){
			console.log('asd');
			$scope.avatarUploader.uploadAll();
		};

		$scope.removeImage = function(image){
			$http.delete('/projects/' + $scope.project._id + '/image/' + image._id)
			.success(function(data){
				loadProject();
			});
		};

		$scope.addLink = function(){
			console.log($scope.newLink);
			$http.post('/projects/' + $scope.project._id + '/link', $scope.newLink)
			.success(function(){
				loadProject();
			});
		};

		$scope.removeLink = function(link){
			$http.delete('/projects/' + $scope.project._id + '/link/' + link._id)
			.success(function(data){
				loadProject();
			});
		};
	}
]);
'use strict';

//Projects service used to communicate Projects REST endpoints
angular.module('projects').factory('Projects', ['$resource',
	function($resource) {
		return $resource('projects/:projectId', { projectId: '@_id'
		}, {
			update: {
				method: 'PUT'
			}
		});
	}
]);
'use strict';

// Config HTTP Error Handling
angular.module('users').config(['$httpProvider',
	function($httpProvider) {
		// Set the httpProvider "not authorized" interceptor
		$httpProvider.interceptors.push(['$q', '$location', 'Authentication',
			function($q, $location, Authentication) {
				return {
					responseError: function(rejection) {
						switch (rejection.status) {
							case 401:
								// Deauthenticate the global user
								Authentication.user = null;

								// Redirect to signin page
								$location.path('signin');
								break;
							case 403:
								// Add unauthorized behaviour 
								break;
						}

						return $q.reject(rejection);
					}
				};
			}
		]);
	}
]);
'use strict';

// Setting up route
angular.module('users').config(['$stateProvider',
	function($stateProvider) {
		// Users state routing
		$stateProvider.
		state('best-positions', {
			url: '/best-positions',
			templateUrl: 'modules/users/views/best-positions.client.view.html'
		}).
		state('user-skills', {
			url: '/skills',
			templateUrl: 'modules/users/views/users-skills.client.view.html'
		}).
		state('user-show', {
			url: '/user/:username',
			templateUrl: 'modules/users/views/user-show.client.view.html'
		}).
		state('profile', {
			url: '/settings/profile',
			templateUrl: 'modules/users/views/settings/edit-profile.client.view.html'
		}).
		state('password', {
			url: '/settings/password',
			templateUrl: 'modules/users/views/settings/change-password.client.view.html'
		}).
		state('accounts', {
			url: '/settings/accounts',
			templateUrl: 'modules/users/views/settings/social-accounts.client.view.html'
		}).
		state('signup', {
			url: '/signup',
			templateUrl: 'modules/users/views/authentication/signup.client.view.html'
		}).
		state('signin', {
			url: '/signin',
			templateUrl: 'modules/users/views/authentication/signin.client.view.html'
		}).
		state('forgot', {
			url: '/password/forgot',
			templateUrl: 'modules/users/views/password/forgot-password.client.view.html'
		}).
		state('reset-invalid', {
			url: '/password/reset/invalid',
			templateUrl: 'modules/users/views/password/reset-password-invalid.client.view.html'
		}).
		state('reset-success', {
			url: '/password/reset/success',
			templateUrl: 'modules/users/views/password/reset-password-success.client.view.html'
		}).
		state('reset', {
			url: '/password/reset/:token',
			templateUrl: 'modules/users/views/password/reset-password.client.view.html'
		});
	}
]);
'use strict';

angular.module('users').controller('AuthenticationController', ['$scope', '$http', '$location', 'Authentication',
	function($scope, $http, $location, Authentication) {
		$scope.authentication = Authentication;

		// If user is signed in then redirect back home
		if ($scope.authentication.user) $location.path('/');

		$scope.signup = function() {
			$http.post('/auth/signup', $scope.credentials).success(function(response) {
				// If successful we assign the response to the global user model
				$scope.authentication.user = response;

				// And redirect to the index page
				$location.path('/');
			}).error(function(response) {
				$scope.error = response.message;
			});
		};

		$scope.signin = function() {
			$http.post('/auth/signin', $scope.credentials).success(function(response) {
				// If successful we assign the response to the global user model
				$scope.authentication.user = response;

				// And redirect to the index page
				$location.path('/');
			}).error(function(response) {
				$scope.error = response.message;
			});
		};
	}
]);
'use strict';

angular.module('users').controller('BestPositionsController', ['$scope', '$http', '$location', 'Authentication',
	function($scope, $http, $location, Authentication) {

		if(!Authentication.user){
			$location.path('/');
		}

		$scope.positions = [];

		$scope.loadPositions = function(){
			$http.get('/users/' + Authentication.user._id + '/best-positions')
			.success(function(data){
				$scope.positions = data;
			});
		};
	}
]);
'use strict';

angular.module('users').controller('PasswordController', ['$scope', '$stateParams', '$http', '$location', 'Authentication',
	function($scope, $stateParams, $http, $location, Authentication) {
		$scope.authentication = Authentication;

		//If user is signed in then redirect back home
		if ($scope.authentication.user) $location.path('/');

		// Submit forgotten password account id
		$scope.askForPasswordReset = function() {
			$scope.success = $scope.error = null;

			$http.post('/auth/forgot', $scope.credentials).success(function(response) {
				// Show user success message and clear form
				$scope.credentials = null;
				$scope.success = response.message;

			}).error(function(response) {
				// Show user error message and clear form
				$scope.credentials = null;
				$scope.error = response.message;
			});
		};

		// Change user password
		$scope.resetUserPassword = function() {
			$scope.success = $scope.error = null;

			$http.post('/auth/reset/' + $stateParams.token, $scope.passwordDetails).success(function(response) {
				// If successful show success message and clear form
				$scope.passwordDetails = null;

				// Attach user profile
				Authentication.user = response;

				// And redirect to the index page
				$location.path('/password/reset/success');
			}).error(function(response) {
				$scope.error = response.message;
			});
		};
	}
]);
'use strict';

angular.module('users').controller('SettingsController', ['$scope', '$http', '$location', '$window', 'Users', 'FileUploader', 'Authentication',
	function($scope, $http, $location, $window, Users, FileUploader, Authentication) {
		$scope.user = Authentication.user;

		//tabs
		$scope.active = 'main';
		$scope.activate = function(value){
			$scope.active = value;
			$scope.success= false;
		};

		$scope.addEducation = function(){
			$scope.user.education.push($scope.newEducation);
			$scope.newEducation = {};
		};

		$scope.removeEducation = function(education){
			$scope.user.education.splice($scope.user.education.indexOf(education), 1);
		};

		$scope.addExperience = function(){
			$scope.user.experience.push($scope.newExperience);
			$scope.newExperience = {};
		};

		$scope.removeExperience = function(experience){
			$scope.user.experience.splice($scope.user.experience.indexOf(experience), 1);
		};


		// If user is not signed in then redirect back home
		if (!$scope.user) $location.path('/');

		// Check if there are additional accounts 
		$scope.hasConnectedAdditionalSocialAccounts = function(provider) {
			for (var i in $scope.user.additionalProvidersData) {
				return true;
			}

			return false;
		};

		// Check if provider is already in use with current user
		$scope.isConnectedSocialAccount = function(provider) {
			return $scope.user.provider === provider || ($scope.user.additionalProvidersData && $scope.user.additionalProvidersData[provider]);
		};

		// Remove a user social account
		$scope.removeUserSocialAccount = function(provider) {
			$scope.success = $scope.error = null;

			$http.delete('/users/accounts', {
				params: {
					provider: provider
				}
			}).success(function(response) {
				// If successful show success message and clear form
				$scope.success = true;
				$scope.user = Authentication.user = response;
			}).error(function(response) {
				$scope.error = response.message;
			});
		};

		// Update a user profile
		$scope.updateUserProfile = function(isValid) {
			if (isValid) {
				$scope.success = $scope.error = null;
				var user = new Users($scope.user);

				user.$update(function(response) {
					$scope.success = true;
					Authentication.user = response;
					$scope.user = response;
				}, function(response) {
					$scope.error = response.data.message;
				});
			} else {
				$scope.submitted = true;
			}
		};

		// Change user password
		$scope.changeUserPassword = function() {
			$scope.success = $scope.error = null;

			$http.post('/users/password', $scope.passwordDetails).success(function(response) {
				// If successful show success message and clear form
				$scope.success = true;
				$scope.passwordDetails = null;
			}).error(function(response) {
				$scope.error = response.message;
			});
		};

		$scope.uploader = new FileUploader({
			url: '/users/avatar'
		});

		$scope.uploadAvatar = function(){
			$scope.uploader.uploadAll();
		};
	}
]);
'use strict';

angular.module('users').controller('UserShowController', ['$scope', '$http', '$window', '$stateParams',
	function($scope, $http, $window, $stateParams) {
		var username = $stateParams.username;

		$scope.width = $window.innerWidth;
		//holds the user to show
		$scope.user = {
			memberOf: [],
			ownerOf: []
		};
		$scope.active = 'personal';
		$scope.loadingUser = true;
		
		$scope.member = {
			page: 1,
			pageCount: 1
		};

		$scope.owner = {
			page: 1,
			pageCount: 1
		};

		$scope.changeMemberPage = function(page){
			$scope.member.page = page;
		};

		$scope.changeOwnerPage = function(page){
			$scope.owner.page = page;
		};

		$scope.getMemberOf = function(page){
			return $scope.user.memberOf.slice((page-1)*5, 5);
		};

		$scope.getOwnerOf = function(page){
			return $scope.user.ownerOf.slice((page-1)*5, 5);
		};

		$scope.activate = function(value){
			$scope.active = value;
		};

		$http.get('/users/' + username).success(function(user){
			$scope.user = user;
			$scope.loadingUser = false;
			$scope.member.pageCount = Math.floor($scope.user.memberOf.length / 5) + 1;
			$scope.owner.pageCount = Math.floor($scope.user.ownerOf.length / 5) + 1;
		}).error(function(data){
			console.log(data);
		});

		angular.element($window).on('resize', function(){
	        $scope.$apply(function(){
	        	$scope.width = $window.innerWidth;
        	});
		});
	}
]);
'use strict';

angular.module('users').controller('UsersSkillsController', ['$scope', '$http', '$location', 'Authentication',
	function($scope, $http, $location, Authentication) {
		var user = Authentication.user;
		console.log(user);
		if (!user) $location.path('/');

		var loadSkills = function(){
			$http.get('/users/' + user.username + '/skills').success(function(data){
				$scope.skills = data;
			});
		};

		$scope.skills = [];
		$scope.newSkill = {};

		loadSkills();		

		$scope.addSkill = function(){
			$http.put('/users/skills/add-skill', $scope.newSkill).success(function(data){
				$scope.newSkill = {};
				Authentication.user = data;
				loadSkills();
			});
		};

		$scope.removeSkill = function(skill){
			$http.put('/users/skills/remove-skill', skill).success(function(data){
				Authentication.user = data;
				loadSkills();
			});
		};


	}
]);
'use strict';

// Authentication service for user variables
angular.module('users').factory('Authentication', [
	function() {
		var _this = this;

		_this._data = {
			user: window.user
		};

		return _this._data;
	}
]);
'use strict';

// Users service used for communicating with the users REST endpoint
angular.module('users').factory('Users', ['$resource',
	function($resource) {
		return $resource('users', {}, {
			update: {
				method: 'PUT'
			}
		});
	}
]);