'use strict';

angular.module('users').controller('InfoController', ['$scope', '$http', '$location', '$window', 'Authentication',
	function($scope, $http, $location, $window, Authentication) {

		if(!Authentication.user){
			$location.path('/');
		}

		$scope.loading = true;
		$scope.info = {};

		$scope.loadInfo = function(){
			$scope.loading = true;
			$http.get('/users/' + Authentication.user._id + '/info')
			.success(function(data){
				$scope.info = data;
				$scope.loading = false;
				$scope.errorMessage = null;
			});
		};

		$scope.updateInfo = function(){
			$http.put('/users/info', $scope.info)
			.success(function(data){
				$scope.loadInfo();
				$scope.successMessage = data.message;
			})
			.error(function(data){
				$scope.successMessage = null;
				$scope.errorMessage = data.message;
			});
		};
	}
]);