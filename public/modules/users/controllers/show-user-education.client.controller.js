'use strict';

angular.module('users').controller('ShowUserEducationController', ['$scope', '$http', '$window',
	function($scope, $http, $window) {

		$scope.loading = true;
		$scope.education = [];

		$scope.loadEducation = function(){
			if($scope.userId){
				$scope.loading = true;
				$http.get('/users/' + $scope.userId + '/education')
				.success(function(data){
					$scope.loading = false;
					$scope.education = data;
				});
			}
		};

		$scope.$watch('userId', function(){
			$scope.loadEducation();
		});		
	}
]);