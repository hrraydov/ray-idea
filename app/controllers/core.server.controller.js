'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
	async = require('async'),
	Project = mongoose.model('Project'),
	User = mongoose.model('User'),
	Skill = mongoose.model('Skill'),
	Position = mongoose.model('Position');
exports.index = function(req, res) {
	res.render('index', {
		user: req.user || null,
		request: req
	});
};

exports.alerts = function(req, res){
	var alerts = [];
	async.parallel([
		function(callback){
			User.count(function(err, count){
				alerts.push({
					value: count,
					order: 1,
					description: 'Потребители',
					color: 'btn btn-info',
					icon: 'glyphicon glyphicon-user'
				});
				callback();
			});
		},
		function(callback){
			Project.count(function(err, count){
				alerts.push({
					value: count,
					order: 2,
					description: 'Проекти',
					color: 'btn btn-success',
					icon: 'glyphicon glyphicon-folder-close'
				});
				callback();
			});
		},
		function(callback){
			Skill.count(function(err, count){
				alerts.push({
					value: count,
					order: 3,
					description: 'Умения',
					color: 'btn btn-primary',
					icon: 'glyphicon glyphicon-book'
				});
				callback();
			});
		},
		function(callback){
			Position.count(function(err, count){
				alerts.push({
					value: count,
					order: 4,
					description: 'Позиции',
					color: 'btn btn-warning',
					icon: 'glyphicon glyphicon-briefcase'
				});
				callback();
			});
		}
		], function(err){
		res.json(alerts);
	});
};