'use strict';

angular.module('projects').controller('ProjectsOwnerController', ['$scope', '$http', '$location', 'Authentication',
	function($scope, $http, $location, Authentication) {
		
		if(!Authentication.user){
			$location.path('/');
		}

		$scope.loading = true;
		$scope.projects = [];
		$scope.page = 1;
		$scope.pageCount = 0;

		$scope.init = function(){
			$scope.loadProjects();
			$scope.loadPageCount();
		};

		$scope.loadProjects = function(){
			$scope.loading = true;
			$http.get('/projects/user/' + Authentication.user._id + '?page=' + $scope.page)
			.success(function(data){
				$scope.loading = false;
				$scope.projects = data;
			});
		};

		$scope.loadPageCount = function(){
			$http.get('/projects/user/' + Authentication.user._id + '/count')
			.success(function(data){
				var count = data.count;
				$scope.pageCount = count / 5 + 1;
			});	
		};

		$scope.nextPage = function(){
			$scope.page += 1;
			$scope.loadProjects();
		};

		$scope.prevPage = function(){
			$scope.page -=1;
			$scope.loadProjects();
		};

		$scope.deleteProject = function(project){
			$http.delete('/projects/' + project._id)
			.success(function(data){
				$scope.loadProjects();
			});
		};
	}
]);