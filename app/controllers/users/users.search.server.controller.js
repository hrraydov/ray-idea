'use strict';

/*
 * Module dependencies 
 */
var mongoose = require('mongoose'),
	async = require('async'),
	User = mongoose.model('User');

exports.search = function(req, res){
	var q = req.query.q;
	var page = req.query.page;
	var result = {};
	async.parallel([
		function(callback){			
			User
			.find({username: new RegExp(q, 'i')})
			.select('username displayName')
			.skip((page-1)*10)
			.limit(10)
			.sort('username')
			.exec(function(err, users){
				result.users = users;
				callback();
			});
		},
		function(callback){
			User.count({username: new RegExp(q, 'i')}, function(err, count){
				result.count = count;
				callback();
			});
		}
	], function(err){
		return res.json(result);
	});
};