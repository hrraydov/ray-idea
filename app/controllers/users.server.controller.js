'use strict';

/**
 * Module dependencies.
 */
var _ = require('lodash');

/**
 * Extend user's controller
 */
module.exports = _.extend(
	require('./users/users.main.server.controller'),
	require('./users/users.authentication.server.controller'),
	require('./users/users.authorization.server.controller'),
	require('./users/users.password.server.controller'),
	require('./users/users.profile.server.controller'),
	require('./users/users.show.server.controller'),
	require('./users/users.skills.server.controller'),
	require('./users/users.positions.server.controller'),
	require('./users/users.info.server.controller'),
	require('./users/users.avatar.server.controller'),
	require('./users/users.education.server.controller'),
	require('./users/users.experience.server.controller'),
	require('./users/users.search.server.controller.js')
);