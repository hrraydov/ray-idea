'use strict';

//Setting up route
angular.module('projects').config(['$stateProvider',
	function($stateProvider) {
		// Projects state routing
		$stateProvider.
		state('projects', {
			url: '/projects',
			templateUrl: 'modules/projects/views/projects.client.view.html'
		}).
		state('create', {
			url: '/projects/create',
			templateUrl: 'modules/projects/views/create-project.client.view.html'
		}).
		state('control-panel', {
			url: '/projects/:projectId/control-panel',
			templateUrl: 'modules/projects/views/control-panel/control-panel.client.view.html'
		}).
		state('control-panel.info', {
			url: '/info',
			templateUrl: 'modules/projects/views/control-panel/control-panel-info.client.view.html'
		});
	}
]);