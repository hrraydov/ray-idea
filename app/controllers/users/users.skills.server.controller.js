'use strict';

/**
 * Module dependencies
 */
var mongoose = require('mongoose'),
	User = mongoose.model('User'),
	Skill = mongoose.model('Skill'),
	_ = require('lodash'),
	async = require('async');

exports.getSkills = function(req, res){
	var user = req.profile;
	return res.json(user.skills);
};

function addSkillToUser(skill, name, user, cb){
	var index;
	for(var i=0; i<user.skills.length; i++){
		if(name.toLowerCase() < user.skills[i].name.toLowerCase()){
			index = i;
			break;
		}
	}
	if(!!index || index === 0){
		user.skills.splice(index, 0,{
			name: name,
			skill: skill._id
		});
	}else{
		user.skills.push({
			name: name,
			skill: skill._id
		});
	}
	user.save(function(err, user){
		cb();
	});
}

exports.addSkill = function(req, res){
	var user = req.user;
	var name = req.body.name;

	Skill.findOne({names: name.toLowerCase()}, function(err, skill){
		if(skill){
			//if skill already exists
			if(skill.users.indexOf(user._id) === -1){
				skill.users.push(user._id);
				skill.save(function(err, skill){
					addSkillToUser(skill, name, user, function(){
						return res.json({});
					});
				});
			}else{
				return res.status(400).send({message: 'That skill already exists'});
			}
		}else{
			//create the skill if it doesn't exist
			var newSkill = new Skill();
			newSkill.names.push(name.toLowerCase());
			newSkill.users.push(user._id);
			newSkill.save(function(err, skill){
				addSkillToUser(skill, name, user, function(){
					return res.json({});
				});
			});
		}
	});
};

exports.removeSkill = function(req, res){
	var user = req.user;
	var bodySkill = req.body;
	Skill.findOne({names: bodySkill.name}).exec(function(err, skill){
		async.parallel([
			function(cb){
				skill.users.pull(user._id);
				skill.save(function(err, skill){
					cb();
				});
			},
			function(cb){
				user.skills.pull(bodySkill);
				user.save(function(err, user){
					cb();
				});
			}
		], function(err){
			return res.json({});
		});		
	});
};