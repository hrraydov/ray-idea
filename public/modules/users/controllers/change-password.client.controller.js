'use strict';

angular.module('users').controller('ChangePasswordController', ['$scope', '$http', '$location', '$window', 'Authentication',
	function($scope, $http, $location, $window, Authentication) {

		if(!Authentication.user){
			$location.path('/');
		}

		$scope.errorMessage = null;
		$scope.successMessage = null;
		$scope.passwordDetails = {};

		$scope.changePassword = function(){
			$scope.successMessage = null;
			$scope.errorMessage = null;

			$http.post('/users/password', $scope.passwordDetails)
			.success(function(response) {
				// If successful show success message and clear form
				$scope.successMessage = response.message;
				$scope.passwordDetails = null;
			}).error(function(response) {
				$scope.errorMessage = response.message;
			});
		};

	}
]);