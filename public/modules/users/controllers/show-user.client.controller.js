'use strict';

angular.module('users').controller('ShowUserController', ['$scope', '$location', '$http', '$window', '$stateParams',
	function($scope, $location, $http, $window, $stateParams) {
		var username = $stateParams.username;

		$scope.userId = null;
		$scope.loading = true;
		$scope.mainPath = '/#!/user/' + username;

		function checkPath(){
			var path = '/#!' + $location.path();
			switch(path){
				case $scope.mainPath:
					$location.path($location.path() + '/info');
					console.log('path changing');
					break;
				case $scope.mainPath + '/info':
					$scope.active = 'info';
					break;
				case $scope.mainPath + '/skills':
					$scope.active = 'skills';
					break;
				case $scope.mainPath + '/education':
					$scope.active = 'education';
					break;
				case $scope.mainPath + '/experience':
					$scope.active = 'experience';
					break;
			}
		}

		$scope.$on('$stateChangeSuccess', function(){
			checkPath();
		});

		$scope.init = function(){
			checkPath();
			$scope.loadUserId();
		};

		$scope.loadUserId = function(){
			$scope.loading = true;
			$http.get('/users/userId/' + username)
			.success(function(data){
				$scope.loading = false;
				$scope.userId = data.userId;
			});
		};
	}
]);