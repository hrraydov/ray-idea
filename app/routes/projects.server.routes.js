'use strict';

module.exports = function(app) {
	var users = require('../../app/controllers/users.server.controller');
	var projects = require('../../app/controllers/projects.server.controller');

	// Main routes
	app.route('/projects/last').get(projects.last);
	app.route('/projects/search').get(projects.search);
	app.route('/projects/:projectId/user/:userId/type').get(projects.getUserType);

	// CRUD routes
	app.route('/projects/user/:userId').get(projects.list);
	app.route('/projects/user/:userId/count').get(projects.count);
	app.route('/projects').post(users.requiresLogin, projects.create);
	app.route('/projects/:projectId')
		.put(users.requiresLogin, projects.hasAuthorization, projects.update)
		.delete(users.requiresLogin, projects.hasAuthorization, projects.delete);

	// Avatar Routes
	app.route('/projects/:projectId/avatar').get(projects.getAvatar);
	app.route('/projects/:projectId/upload-avatar').post(users.requiresLogin, projects.hasAuthorization, projects.uploadAvatar);
	app.route('/projects/:projectId/change-avatar-link').post(users.requiresLogin, projects.hasAuthorization, projects.changeAvatarLink);
	
	// Info routes
	app.route('/projects/:projectId/info')
		.get(projects.getInfo)
		.put(users.requiresLogin, projects.hasAuthorization, projects.updateInfo);

	// Membership routes	

	// Positions routes	

	// Resources routes
	
	app.param('projectId', projects.projectById);
	app.param('userId', users.userByID);
};
