'use strict';

angular.module('users').controller('ShowUserInfoController', ['$scope', '$http', '$window',
	function($scope, $http, $window, $stateParams) {

		$scope.width = $window.innerWidth;
		$scope.info = {};
		$scope.loading = true;

		$scope.loadInfo = function(){
			if($scope.userId){
				$scope.loading = true;
				$http.get('/users/' + $scope.userId + '/info')
				.success(function(data){
					$scope.loading = false;
					$scope.info = data;
				});
			}
		};

		$scope.$watch('userId', function(){
			$scope.loadInfo();
		});	

		angular.element($window).on('resize', function(){
			$scope.width = $window.innerWidth;
			$scope.$apply();
		});
	}
]);