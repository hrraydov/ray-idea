'use strict';

angular.module('projects').controller('CreateProjectController', ['$scope', '$http', '$location', 'Authentication',
	function($scope, $http, $location, Authentication) {
		
		if(!Authentication.user){
			$location.path('/');
		}

		$scope.project = {
			public: 'true',
			type: 'web'
		};
		$scope.errorMessage = null;

		$scope.create = function(){
			$http.post('/projects', $scope.project)
			.success(function(data){
				$location.path('/projects/' + data._id + '/control-panel');
			})
			.error(function(data){
				$scope.errorMessage = data.message;
			});
		};
	}
]);