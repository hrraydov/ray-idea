'use strict';

angular.module('users').controller('ShowUserExperienceController', ['$scope', '$http', '$window',
	function($scope, $http, $window) {

		$scope.loading = true;
		$scope.experience = [];

		$scope.loadExperience = function(){
			if($scope.userId){
				$scope.loading = true;
				$http.get('/users/' + $scope.userId + '/experience')
				.success(function(data){
					$scope.loading = false;
					$scope.experience = data;
				});
			}
		};

		$scope.$watch('userId', function(){
			$scope.loadExperience();
		});		
	}
]);