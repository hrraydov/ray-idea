'use strict';

/**
 * Module dependencies
 */
var _ = require('lodash');

/**
 * Project updated
 */
exports.emitProjectUpdated = function(req, project){
	var membersAndOwner = project.members;
	membersAndOwner.push(project.user);
	var sockets = req.app.get('sockets');
	_.forEach(membersAndOwner, function(userId){
		if(sockets[userId]){
			sockets[userId].emit('project:' + project._id + '.updated');
		}
	});
};