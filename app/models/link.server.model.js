'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
	Schema = mongoose.Schema;

/**
 * Link Schema
 */
var LinkSchema = new Schema({
	url: {
		type: String,
		required: 'url is required'
	},
	title: {
		type: String,
		required: 'Title is required'
	},
	by: {
		type: Schema.Types.ObjectId,
		ref: 'User'
	},
	project: {
		type: Schema.Types.ObjectId,
		ref: 'Project'
	}
});

mongoose.model('Link', LinkSchema);