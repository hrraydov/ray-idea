'use strict';

angular.module('projects').controller('ControlPanelInfoController', ['$scope', '$http', '$window',
	function($scope, $http, $window, $stateParams) {

		$scope.info = {};
		$scope.loading = true;
		$scope.errorMessage = null;
		$scope.successMessage = null;

		$scope.loadInfo = function(){
			$scope.loading = true;
			$http.get('/projects/' + $scope.projectId + '/info')
			.success(function(data){
				$scope.loading = false;
				$scope.info = data;
			});
		};

		$scope.save = function(){
			$http.put('/projects/' + $scope.projectId + '/info', $scope.info)
			.success(function(data){
				$scope.successMessage = data.message;
				$scope.errorMessage = null;
				$scope.loadInfo();
			})
			.error(function(data){
				$scope.successMessage = null;
				$scope.errorMessage = data.message;
			});
		};

		$scope.$on('project.updated', function(){
			$scope.loadInfo();
		});
	}
]);