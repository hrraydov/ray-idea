'use strict';

/**
 * Module dependencies
 */

var Education = require('mongoose').model('Education'),
	User = require('mongoose').model('User'),
	errorHandler = require('../errors.server.controller.js'),
	async = require('async'),
	_ = require('lodash');

/**
 * Get education by userId
 */
exports.getEducation = function(req, res){
	var user = req.profile;
	Education.find({user: user._id}).sort('-startYear').exec(function(err, collection){
		return res.json(collection);
	});
};

/**
 * Adding education
 */
exports.addEducation = function(req, res){
	var user = req.user;
	var education = new Education(req.body);
	education.user = user._id;
	education.save(function(err, education){
		if(err){
			return res.status(400).send({message: errorHandler.getErrorMessage(err)});
		}
		user.education.push(education._id);
		user.save(function(err){
			return res.json({});
		});
	});
};

/**
 * Removing education
 */
exports.removeEducation = function(req, res){
	var user = req.user;
	var education = req.education;
	
	async.parallel([
		function(cb){
			education.remove(function(err){
				cb();
			});
		},
		function(cb){
			user.education.pull(education._id);
			user.save(function(err){
				cb();
			});
		}
	], function(err){
		return res.json({});
	});
};

/**
 * Editting education
 */
exports.editEducation = function(req, res){
	var user = req.user;
	var education = req.education;

	education = _.assign(education, req.body);
	education.save(function(err){
		if(err){
			return res.status(400).send({message: errorHandler.getErrorMessage(err)});
		}
		return res.json({});
	});
};

/**
 * Education middleware
 */
exports.educationById = function(req, res, next, id){
	Education.findById(id, function(err, education){
		if(!education){
			return res.status(404).send({message: 'Education not found'});
		}
		req.education = education;
		next();
	});
};

exports.educationOfUser = function(req, res, next){
	var education = req.education;
	var user = req.user;
	console.log(education);
	console.log(user);
	if(!education.user.equals(user._id)){
		return res.status(403).send({message: 'Not authorized'});
	}
	next();
};