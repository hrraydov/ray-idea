'use strict';

/**
 * Module dependencies
 */
var Experience = require('mongoose').model('Experience'),
	User = require('mongoose').model('User'),
	errorHandler = require('../errors.server.controller.js'),
	async = require('async'),
	_ = require('lodash');

/**
 * Get experience by userId
 */
exports.getExperience = function(req, res){
	var user = req.profile;
	Experience.find({user: user._id}).sort('-startYear').exec(function(err, collection){
		return res.json(collection);
	});
};

/**
 * Adding experience
 */
exports.addExperience = function(req, res){
	var user = req.user;
	var experience = new Experience(req.body);
	experience.user = user._id;
	experience.save(function(err, experience){
		if(err){
			return res.status(400).send({message: errorHandler.getErrorMessage(err)});
		}
		user.experience.push(experience._id);
		user.save(function(err){
			return res.json({});
		});
	});
};

/**
 * Removing experience
 */
exports.removeExperience = function(req, res){
	var user = req.user;
	var experience = req.experience;
	
	async.parallel([
		function(cb){
			experience.remove(function(err){
				cb();
			});
		},
		function(cb){
			user.experience.pull(experience._id);
			user.save(function(err){
				cb();
			});
		}
	], function(err){
		return res.json({});
	});
};

/**
 * Editting experience
 */
exports.editExperience = function(req, res){
	var user = req.user;
	var experience = req.experience;

	experience = _.assign(experience, req.body);
	experience.save(function(err){
		if(err){
			return res.status(400).send({message: errorHandler.getErrorMessage(err)});
		}
		return res.json({});
	});
};

/**
 * Experience middleware
 */
exports.experienceById = function(req, res, next, id){
	Experience.findById(id, function(err, experience){
		if(!experience){
			return res.status(404).send({message: 'Experience not found'});
		}
		req.experience = experience;
		next();
	});
};

exports.experienceOfUser = function(req, res, next){
	var experience = req.experience;
	var user = req.user;
	if(!experience.user.equals(user._id)){
		return res.status(403).send({message: 'Not authorized'});
	}
	next();
};