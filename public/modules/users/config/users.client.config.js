'use strict';

// Config HTTP Error Handling
angular.module('users').config(['$httpProvider',
	function($httpProvider) {
		// Set the httpProvider "not authorized" interceptor
		$httpProvider.interceptors.push(['$q', '$location', 'Authentication',
			function($q, $location, Authentication) {
				return {
					responseError: function(rejection) {
						switch (rejection.status) {
							case 401:
								// Deauthenticate the global user
								Authentication.user = null;

								// Redirect to signin page
								$location.path('signin');
								break;
							case 403:
								// Add unauthorized behaviour 
								break;
						}

						return $q.reject(rejection);
					}
				};
			}
		]);
	}
]);

angular.module('users').run(['$window', 'Socket', 'Authentication',
	function($window, Socket, Authentication){
		Socket.on('user.updated', function(user) {
		    $window.user = user;
		    Authentication.user = user;
		});
		Socket.on('socket.connected', function(){
			if(Authentication.user){
				Socket.emit('user.login', Authentication.user._id);
			}
		});
	}
]);