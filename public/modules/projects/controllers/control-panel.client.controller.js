'use strict';

angular.module('projects').controller('ControlPanelController', ['$scope', '$location', '$http', '$stateParams', 'Socket', 'Authentication',
	function($scope, $location, $http, $stateParams, Socket, Authentication) {

		$scope.loading = true;
		$scope.projectId = $stateParams.projectId;
		$scope.mainPath = '/#!/projects/' + $scope.projectId + '/control-panel';
		$scope.isMember = false;
		$scope.isOwner = false;

		if(!Authentication.user){
			$location.path('/');
		}

		function checkPath(){
			var path = '/#!' + $location.path();
			console.log(path);
			console.log($scope.mainPath);
			switch(path){
				case $scope.mainPath:
					$location.path($location.path() + '/info');
					break;
				case $scope.mainPath + '/info':
					$scope.active = 'info';
					break;
				case $scope.mainPath + '/avatar':
					$scope.active = 'avatar';
					break;
				case $scope.mainPath + '/resources':
					$scope.active = 'resources';
					break;
				case $scope.mainPath + '/members':
					$scope.active = 'members';
					break;
				case $scope.mainPath + '/candidates':
					$scope.active = 'candidates';
					break;
				case $scope.mainPath + '/invitations':
					$scope.active = 'invitations';
					break;
				case $scope.mainPath + '/positions':
					$scope.active = 'positions';
					break;
			}
		}

		$scope.$on('$stateChangeSuccess', function(){
			checkPath();
		});

		$scope.init = function(){
			$http.get('/projects/' + $scope.projectId + '/user/' + Authentication.user._id + '/type')
			.success(function(data){
				var type = data.type;
				if(type === 'member'){
					$scope.isMember = true;
					checkPath();
				}else if(type === 'owner'){
					$scope.isOwner = true;
					checkPath();
				}else{					
					$location.path('/');
				}
			})
			.error(function(){
				$location.path();
			});
		};

		Socket.on('project:' + $scope.projectId + '.updated', function(){
			$scope.$broadcast('project.updated');
		});
	}
]);
