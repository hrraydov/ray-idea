'use strict';

/**
 * Module Dependencies
 */
var mongoose = require('mongoose'),
	User = mongoose.model('User');

/**
 * Get userId by username
 */
exports.getUserId = function(req, res){
	var username = req.params.username;

	User.findOne({username: username}).exec(function(err, user){
		if(err){
			return res.status(400).send(err);
		}
		if(!user){
			return res.status(404).send({message: 'User not found'});
		}
		return res.status(200).json({userId: user._id});
	});
};