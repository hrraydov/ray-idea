'use strict';

angular.module('users').controller('SkillsController', ['$scope', '$http', '$location', 'Authentication',
	function($scope, $http, $location, Authentication) {
		var user = Authentication.user;

		if (!user) $location.path('/');

		$scope.loading = true;
		$scope.skills = [];
		$scope.newSkill = {};

		$scope.loadSkills = function(){
			$scope.loading = true;
			$http.get('/users/' + user._id + '/skills')
			.success(function(data){
				$scope.skills = data;
				$scope.loading = false;
			});
		};	

		$scope.addSkill = function(){
			$http.put('/users/skills/add-skill', $scope.newSkill)
			.success(function(data){
				$scope.newSkill = {};
				$scope.loadSkills();
			});
		};

		$scope.removeSkill = function(skill){
			$http.put('/users/skills/remove-skill', skill)
			.success(function(data){
				$scope.loadSkills();
			});
		};


	}
]);