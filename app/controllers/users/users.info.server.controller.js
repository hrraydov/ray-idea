'use strict';

/**
 * Module dependencies
 */

var Education = require('mongoose').model('Education'),
	User = require('mongoose').model('User'),
	errorHandler = require('../errors.server.controller.js'),
	async = require('async'),
	_ = require('lodash');

/**
 * Get information by userId
 */
exports.getInfo = function(req, res){
	var user = req.profile;
	return res.json(user.getInfo());
};

/**
 * Update user information
 */
exports.updateInfo = function(req, res){
	var user = req.user;
	var socketio = req.app.get('socketio');
	var sockets = req.app.get('sockets');

	//delete given roles for security reasons
	delete req.body.roles;

	user = _.assign(user, req.body);
	user.displayName = user.firstName + ' ' + user.lastName;
	user.save(function(err, user){
		if(err){
			return res.status(400).send({message: errorHandler.getErrorMessage(err)});
		}
		sockets[user._id].emit('user.updated', user);
		return res.json({message: 'Информацията е запазена успешно'});
	});
};