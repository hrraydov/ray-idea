'use strict';

/**
 * Module dependencies
 */

var User = require('mongoose').model('User'),
	errorHandler = require('../errors.server.controller.js'),
	async = require('async'),
	multiparty = require('multiparty'),
	cloudinary = require('cloudinary'),
	_ = require('lodash');

/**
 * Get user avatar
 */
exports.getAvatar = function(req, res){
	var user = req.profile;
	console.log(user.avatar);
	return res.status(200).send(user.avatar);
};

/**
 * Upload new avatar
 */
exports.uploadAvatar = function(req, res){
	var form = new multiparty.Form();
	var user = req.user;
    form.parse(req, function(err, fields, files) {
    	var file = files.file[0];
    	var contentType = file.headers['content-type'];
    	var extension = file.path.substring(file.path.lastIndexOf('.'));
    	if(!/jpg|jpeg|png/.test(extension)){
    		return res.status(400).send({
    			message: 'Only JPEG ang PNG files'
    		});
    	}
    	async.waterfall([
    		function(cb){
    			if(user.avatar !== '/modules/users/img/default_avatar.jpg'){
					cloudinary.api.delete_resources([user._id],
	    			function(result){ cb(); }, { keep_original: true });
				}
    		},
    		function(cb){    			
				cloudinary.uploader.upload(file.path, function(result){					
					user.avatar = result.url;
					user.save(function(err, user){
						cb();
					});
				}, {public_id: req.user._id, angle: 'exif'});
    		}
    	], function(err){
    		return res.json({});
    	});
    });
};

/**
 * Change avatar link
 */
exports.changeAvatarLink = function(req, res){
	var user = req.user;
	var link = req.body.link;

	if(user.avatar !== '/modules/users/img/default_avatar.jpg'){
		cloudinary.api.delete_resources([user._id],
		function(result){}, { keep_original: true });
	}
	user.avatar = link;
	user.save(function(err, user){
		return res.json({});
	});
};