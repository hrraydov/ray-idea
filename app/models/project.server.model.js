'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
	Schema = mongoose.Schema;

/**
 * Project Schema
 */
var ProjectSchema = new Schema({
	avatar: {
		type: String,
		default: '/modules/users/img/default_avatar.jpg'
	},
	name: {
		type: String,
		default: '',
		required: 'Попълни име на проекта',
		trim: true
	},
	created: {
		type: Date,
		default: Date.now
	},
	user: {
		type: Schema.Types.ObjectId,
		ref: 'User',
		required: true
	},
	type: {
		type: String,
		enum: ['web', 'mobile', 'desktop'],
		default: 'web'
	},
	public: {
		type: Boolean,
		default: true
	},
	description: {
		type: String,
		default: '',
		required: 'Попълни описание',
		trim: true
	},
	urlName: {
		type: String,
		index: true
	},
	positions: {
		type: [{
			type: Schema.Types.ObjectId,
			ref: 'Position'
		}],
		default: []
	},
	images: {
		type: [{
			type: Schema.Types.ObjectId,
			ref: 'Image'
		}],
		default: []
	},
	links: {
		type: [{
			type: Schema.Types.ObjectId,
			ref: 'Link'
		}],
		default: []
	},
	members: {
		type: [{
			type: Schema.Types.ObjectId,
			ref: 'User'
		}],
		default: []
	},
	candidates: {
		type: [{
			type: Schema.Types.ObjectId,
			ref: 'User'
		}],
		default: []
	},
	invitations: {
		type: [{
			type: Schema.Types.ObjectId,
			ref: 'User'
		}],
		default: []
	},
});

ProjectSchema.methods.getPublicData = function(){
	if(this.type === 'private'){
		return {
			_id: this._id,
			user: this.user,
			name: this.name,
			description: this.description,
			type: this.type
		};
	}else{
		return {
			_id: this._id,
			user: this.user,
			name: this.name,
			description: this.description,
			type: this.type,
			members: this.members,
			positions: this.positions,
			images: this.images,
			avatar: this.avatar
		};
	}
};

ProjectSchema.methods.getInfo = function(){
	return {
		_id: this._id,
		user: this.user,
		name: this.name,
		description: this.description,
		type: this.type,
		public: this.public
	};
};

mongoose.model('Project', ProjectSchema);