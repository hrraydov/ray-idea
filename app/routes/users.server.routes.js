'use strict';

/**
 * Module dependencies.
 */
var passport = require('passport');

module.exports = function(app) {
	// User Routes
	var users = require('../../app/controllers/users.server.controller');

	// Search Routes
	app.route('/users/search').get(users.search);

	// Positions Routes
	app.route('/users/:userId/best-positions').get(users.bestPositions);

	// Show user
	app.route('/users/userId/:username').get(users.getUserId);

	// Avatar Routes
	app.route('/users/:userId/avatar').get(users.getAvatar);
	app.route('/users/upload-avatar').post(users.requiresLogin, users.uploadAvatar);
	app.route('/users/change-avatar-link').post(users.requiresLogin, users.changeAvatarLink);
	
	// Info routes
	app.route('/users/:userId/info').get(users.getInfo);
	app.route('/users/info').put(users.requiresLogin, users.updateInfo);

	// Education routes
	app.route('/users/:userId/education').get(users.getEducation);
	app.route('/education').post(users.requiresLogin, users.addEducation);
	app.route('/education/:educationId')
		.put(users.requiresLogin, users.educationOfUser, users.editEducation)
		.delete(users.requiresLogin, users.educationOfUser, users.removeEducation);

	// Experience routes
	app.route('/users/:userId/experience').get(users.getExperience);
	app.route('/experience').post(users.requiresLogin, users.addExperience);
	app.route('/experience/:experienceId')
		.put(users.requiresLogin, users.experienceOfUser, users.editExperience)
		.delete(users.requiresLogin, users.experienceOfUser, users.removeExperience);

	// User skills api
	app.route('/users/:userId/skills').get(users.getSkills);
	app.route('/users/skills/add-skill').put(users.requiresLogin, users.addSkill);
	app.route('/users/skills/remove-skill').put(users.requiresLogin, users.removeSkill);

	// Setting up the users profile api
	app.route('/users/me').get(users.me);
	app.route('/users').put(users.update);
	app.route('/users/accounts').delete(users.removeOAuthProvider);

	// Setting up the users password api
	app.route('/users/password').post(users.changePassword);
	app.route('/auth/forgot').post(users.forgot);
	app.route('/auth/reset/:token').get(users.validateResetToken);
	app.route('/auth/reset/:token').post(users.reset);

	// Setting up the users authentication api
	app.route('/auth/signup').post(users.signup);
	app.route('/auth/signin').post(users.signin);
	app.route('/auth/signout').get(users.signout);

	// Setting the facebook oauth routes
	app.route('/auth/facebook').get(passport.authenticate('facebook', {
		scope: ['email']
	}));
	app.route('/auth/facebook/callback').get(users.oauthCallback('facebook'));

	// Setting the twitter oauth routes
	app.route('/auth/twitter').get(passport.authenticate('twitter'));
	app.route('/auth/twitter/callback').get(users.oauthCallback('twitter'));

	// Setting the google oauth routes
	app.route('/auth/google').get(passport.authenticate('google', {
		scope: [
			'https://www.googleapis.com/auth/userinfo.profile',
			'https://www.googleapis.com/auth/userinfo.email'
		]
	}));
	app.route('/auth/google/callback').get(users.oauthCallback('google'));

	// Setting the linkedin oauth routes
	app.route('/auth/linkedin').get(passport.authenticate('linkedin'));
	app.route('/auth/linkedin/callback').get(users.oauthCallback('linkedin'));

	// Setting the github oauth routes
	app.route('/auth/github').get(passport.authenticate('github'));
	app.route('/auth/github/callback').get(users.oauthCallback('github'));

	// Finish by binding the user middleware
	app.param('userId', users.userByID);
	app.param('educationId', users.educationById);
	app.param('experienceId', users.experienceById);
};