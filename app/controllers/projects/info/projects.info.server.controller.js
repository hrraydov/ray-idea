'use strict';

/**
 * Module dependencies
 */
var Project = require('mongoose').model('Project'),
	User = require('mongoose').model('User'),
	projectSockets = require('./../projects.sockets.server.controller.js'),
	errorHandler = require('./../../errors.server.controller.js'),
	async = require('async'),
	_ = require('lodash');


/**
 * Get information for project
 */
exports.getInfo = function(req, res){
	var project = req.project;

	return res.json(project.getInfo());
};

/**
 * Update information for project
 */
exports.updateInfo = function(req, res){
	var project = req.project;

	project = _.assign(project, req.body);

	project.save(function(err, project){
		if(err){
			return res.status(400).send({message: errorHandler.getErrorMessage(err)});
		}
		projectSockets.emitProjectUpdated(req, project);
		return res.json({message: 'The project is saved successfully'});
	});
};