'use strict';

angular.module('users').controller('ExperienceController', ['$scope', '$http', '$location', '$window', 'Authentication',
	function($scope, $http, $location, $window, Authentication) {

		if(!Authentication.user){
			$location.path('/');
		}

		$scope.loading = true;
		$scope.experience = [];
		$scope.newExperience = {};
		$scope.errorMessage = null;

		$scope.loadExperience = function(){
			$scope.loading = true;
			$http.get('/users/' + Authentication.user._id + '/experience')
			.success(function(data){
				$scope.experience = data;
				$scope.loading = false;
				$scope.errorMessage = null;
			});
		};

		$scope.addExperience = function(){
			$http.post('/experience', $scope.newExperience)
			.success(function(data){
				$scope.loadExperience();
				$scope.newExperience = {};
			})
			.error(function(data){
				$scope.errorMessage = data.message;
			});
		};

		$scope.removeExperience = function(experience){
			$http.delete('/experience/' + experience._id)
			.success(function(data){
				$scope.loadExperience();
			});
		};

		$scope.editExperience = function(experience){
			$http.put('/education/' + experience._id, experience)
			.success(function(data){
				$scope.loadExperience();
			})
			.error(function(data){
				$scope.errorMessage = data.message;
			});
		};
	}
]);