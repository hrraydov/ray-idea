'use strict';

angular.module('users').controller('ShowUserSkillsController', ['$scope', '$http', '$window',
	function($scope, $http, $window, $stateParams) {

		$scope.width = $window.innerWidth;
		$scope.skills = [];

		$scope.loadSkills = function(){
			if($scope.userId){
				$scope.loading = true;
				$http.get('/users/' + $scope.userId + '/skills')
				.success(function(data){
					$scope.loading = false;
					$scope.skills = data;
				});
			}
		};

		$scope.$watch('userId', function(){
			$scope.loadSkills();
		});		
	}
]);