'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
	Schema = mongoose.Schema;

/**
 * Image Schema
 */
var ImageSchema = new Schema({
	url: {
		type: String,
		required: 'image url is required'
	},
	by: {
		type: Schema.Types.ObjectId,
		ref: 'User'
	},
	project: {
		type: Schema.Types.ObjectId,
		ref: 'Project'
	}
});

mongoose.model('Image', ImageSchema);